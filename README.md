This will be my blog.

It will be

- made with a static site generator (probably Lektor)
- pragmatic
- written in Markdown
- fast and standards-compliant
- generated and deployed via CI

Future extensions will add

- search (maybe with a pre-generated index by Lunr.js)
