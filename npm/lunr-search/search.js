const lunr = require("lunr");
require("lunr-languages/lunr.stemmer.support.js")(lunr);
require("lunr-languages/lunr.de.js")(lunr);
require("lunr-languages/lunr.multi.js")(lunr);

import Vue from 'vue';

lunr.multiLanguage('en', 'de');

let index;

const loadIndex = async function () {
  const response = await fetch(window.env.LUNR_INDEX_URL);
  index = lunr.Index.load(await response.json());
  console.log(index);
}

let app = new Vue({
  el: '#lunr-search',
  template: `
  <div class="lunr-search">
    <label for="searchTerm" class="visually-hidden">Search term</label>
    <input type="text" v-model="searchTerm" id="searchTerm" placeholder="Search"/>
    <div v-if="displayResults" class="lunr-search--results shadowed">
      <p>{{ searchResults.length > 0 ? searchResults.length : "No" }} result{{ searchResults.length != 1 ? "s" : "" }}.</p>
      <ul>
        <li v-for="result in searchResults">
          <a
            v-bind:href="'/' + result.ref"
            v-if="pageTitleCache[result.ref]"
          >
          {{ pageTitleCache[result.ref] }}
          </a>
          <span v-if="!pageTitleCache[result.ref]">Loading...</span>
        </li>
      </ul>
    </div>
  </div>`,
  data: function () {
    return {
      searchTerm: "",
      index: null,
      pageTitleCache: {},
    };
  },
  mounted: function () {
    this.loadIndex();
  },
  methods: {
    loadIndex: async function () {
      const response = await fetch(window.env.LUNR_INDEX_URL);
      this.index = lunr.Index.load(await response.json());
    },
    getPageTitle: async function (url) {
      const response = await fetch(url);
      const text = await response.text()
      const parser = new DOMParser();
      const doc = parser.parseFromString(text, "text/html");
      // return doc.title;
      return doc.getElementsByTagName("h2")[0].textContent;
    }
  },
  computed: {
    searchQuery: function () {
      const trimmedInput = this.searchTerm.trim();
      if (trimmedInput === "") {
        return "";
      }

      const individualTerms = trimmedInput.split(" ");
      let terms = [];

      individualTerms.forEach((term) => {
        terms.push(`${term}`);   // Complete match
        terms.push(`${term}*`);  // Partial match at the beginning of the word
        terms.push(`${term}~1`); // Fuzzy match with editing distance 1
      });

      return terms.join(" ");
    },
    searchResults: function () {
      if (!this.index || !this.searchTerm) {
        return [];
      }
      return this.index.search(this.searchQuery);
    },
    displayResults: function () {
      return this.searchQuery !== ""
    }
  },
  watch: {
    searchResults: async function (searchResults) {
      searchResults.forEach(async (searchResult) => {
        if (!this.pageTitleCache[searchResult.ref]) {
          this.$set(this.pageTitleCache, searchResult.ref, await this.getPageTitle("/" + searchResult.ref));
        }
      });
    }
  }
});
