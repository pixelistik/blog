console.log("Building index");

const lunr = require("lunr");
require("lunr-languages/lunr.stemmer.support.js")(lunr);
require("lunr-languages/lunr.de.js")(lunr);
require("lunr-languages/lunr.multi.js")(lunr);

const fs = require("fs");
var glob = require("glob");

const INDEX_OUTPUT_FILE =
  __dirname + "/../../assets/static/gen/lunr-index.json";

glob(__dirname + "/../../content/*/contents.lr", function(err, files) {
  files.forEach(file => {
    let id = file.match(new RegExp("/([A-Za-z0-9-_]+)/contents\.lr"))[1];
    console.log(id);
    let fileContent = fs.readFileSync(file) + "";
    let doc = {
      id: id,
      content: fileContent
    };
    docs.push(doc);
  });

  const index = lunr(function() {
    this.use(lunr.multiLanguage('en', 'de'));
    // this.field("title", {boost: 100});
    // this.field("headings", {boost: 10});
    this.ref("id");
    this.field("content");

    docs.forEach(doc => {
      this.add(doc);
    });
  });

  fs.writeFileSync(INDEX_OUTPUT_FILE, JSON.stringify(index));
});

let docs = [];
